## BotC Wallet Frontend  

A [React](https://facebook.github.io/react) & [Redux](https://react-redux.js.org/) powered implementation of
[BankOfTheCommons Wallet](https://wallet.bankofthecommons.coop) using its
[API](https://doc.bankofthecommons.coop).

Created with *create-react-app*. See the [full create-react-app guide](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## How to run

The project is using [Yarn](https://yarnpkg.com/lang/en/) as package manager.

Before running the commands, make sure there is a `.env` file at the root of the project with the variables specified in `.env.example` set to the correct values.

### `yarn install`  
  
To install all node packages required by the project.

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
  
### `yarn build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
App is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
