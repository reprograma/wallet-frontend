const BACKEND_URL = process.env.REACT_APP_BACKEND_URL;
export const tokenName = 'auth';

export function getAuthToken() {
  if (!window.localStorage[tokenName]) {
    return undefined;
  }
  return window.localStorage[tokenName];
}

export function logOut() {
  window.localStorage.removeItem(tokenName);
}

export function isLoggedIn() {
  return Boolean(getAuthToken());
}

function handleFetchErrors(response) {
  if (!response.ok) {
    const error = new Error(response.statusText);
    error.response = response;
    return Promise.reject(error);
  }
  return response;
}

export function fetchApi(args) {
  const { path, body, method } = args;
  const url = `${BACKEND_URL}/${path}`;

  const newargs = {
    body: JSON.stringify(body),
    method: method || 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  };

  if (isLoggedIn()) {
    newargs.headers.Authorization = `Bearer ${getAuthToken()}`;
  }

  return fetch(url, newargs)
    .then(handleFetchErrors)
    .then(response => response.json())
    .catch(async error => {
      if (!error.response) {
        throw error;
      }
      if (error.response.status === 401 && isLoggedIn()) {
        logOut();
        window.history.go('/login');
      }
      const apiErrorResponse = await error.response.json();
      apiErrorResponse.status = error.response.status;
      apiErrorResponse.statusText = error.response.statusText;
      throw Object.assign(new Error(), apiErrorResponse);
    });
}
