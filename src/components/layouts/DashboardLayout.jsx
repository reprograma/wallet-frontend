import React from 'react';
import cx from 'classnames';
import { connect } from 'react-redux';

import { Switch, Route, Redirect } from 'react-router-dom';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

// core components
import Header from 'components/ui/Header/Header';
import Sidebar from 'components/ui/Sidebar';

import dashboardRoutes from 'components/routes/dashboard-routes';

import appStyle from 'assets/jss/material-dashboard-react/layouts/dashboardStyle';

import image from 'assets/img/sidebar-2.jpg';
import logo from 'logo.jpg';

import { getUser } from 'store/user/actions';
import { getWallet } from 'store/wallet/actions';

const switchRoutes = (
  <Switch>
    {dashboardRoutes.map(prop => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.pathTo} key={prop.id} />;
      if (prop.collapse)
        return prop.views.map(prop => {
          return (
            <Route path={prop.path} component={prop.component} key={prop.id} />
          );
        });
      return (
        <Route path={prop.path} component={prop.component} key={prop.id} />
      );
    })}
  </Switch>
);

class DashboardLayout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mobileOpen: false,
      miniActive: false
    };

    this.mainPanelRef = React.createRef();

    this.getRoute = this.getRoute.bind(this);
    this.handleDrawerToggle = this.handleDrawerToggle.bind(this);
    this.sidebarMinimize = this.sidebarMinimize.bind(this);
    this.resizeFunction = this.resizeFunction.bind(this);
    this.getDashboardData = this.getDashboardData.bind(this);
  }

  componentDidMount() {
    this.getDashboardData([this.props.getUser, this.props.getWallet]);
  }

  // TODO: fix me
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.status === 'error') {
      this.props.history.push('/login');
    }
  }

  // TODO: check this - what is it for?
  getRoute = () => {
    return this.props.location.pathname !== '/maps/full-screen-maps';
  };

  getDashboardData = (fetchers = [() => {}]) => {
    fetchers.forEach(f => f());
  };

  handleDrawerToggle = () => {
    this.setState(prevState => ({ mobileOpen: !prevState.mobileOpen }));
  };

  sidebarMinimize = () => {
    this.setState(prevState => ({ miniActive: !prevState.miniActive }));
  };

  resizeFunction = () => {
    if (window.innerWidth >= 960) {
      this.setState({ mobileOpen: false });
    }
  };

  render() {
    const { classes, user, status, walletStatus, ...rest } = this.props;
    if (status === 'pending' && walletStatus === 'pending') {
      return <div>Loading</div>;
    }
    const mainPanel = `${classes.mainPanel} ${cx({
      [classes.mainPanelSidebarMini]: this.state.miniActive,
      [classes.mainPanelWithPerfectScrollbar]:
        navigator.platform.indexOf('Win') > -1
    })}`;
    return (
      <div className={classes.wrapper}>
        <Sidebar
          routes={dashboardRoutes}
          logoText="BotC Wallet"
          logo={logo}
          image={image}
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
          color="green"
          bgColor="black"
          miniActive={this.state.miniActive}
          user={user}
          {...rest}
        />
        <div className={mainPanel} ref={this.mainPanelRef}>
          <Header
            sidebarMinimize={this.sidebarMinimize}
            miniActive={this.state.miniActive}
            routes={dashboardRoutes}
            handleDrawerToggle={this.handleDrawerToggle}
            {...rest}
          />
          <div className={classes.content}>
            <div className={classes.container}>{switchRoutes}</div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.user,
  status: state.user.status,
  walletStatus: state.wallet.status
});

export default connect(mapStateToProps, { getUser, getWallet })(
  withStyles(appStyle)(DashboardLayout)
);
