import React, { Component } from 'react';
import { connect } from 'react-redux';

import CardHeader from 'components/ui/Card/CardHeader';
import CardBody from 'components/ui/Card/CardBody';
import Button from 'components/ui/Button';

import { Popover, Paper } from '@material-ui/core';
import Refresh from '@material-ui/icons/Refresh';

import * as actions from 'store/earnings/actions';

class Earnings extends Component {
  constructor() {
    super();
    this.state = {
      anchorEl: null
    };
  }

  componentDidMount() {
    this.props.getEarnings();
  }

  handlePopoverOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handlePopoverClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { earnings, status, error } = this.props;
    const { anchorEl } = this.state;

    let earningsInfo;

    if (status === 'pending') {
      earningsInfo = <div>Loading</div>;
    }

    if (status === 'error') {
      earningsInfo = <div>{error.error_description}</div>;
    }

    if (status === 'success' && !Object.keys(earnings)) {
      earningsInfo = <div>No data found</div>;
    } else {
      const {
        day = 0,
        week = 0,
        month = 0,
        currency = 'EUR',
        scale = 0
      } = earnings;
      earningsInfo = (
        <>
          <div>
            Day: {day / (10 ** scale).toFixed(scale)} {currency}
          </div>
          <div>
            Week: {week / (10 ** scale).toFixed(scale)} {currency}
          </div>
          <div>
            Month: {month / (10 ** scale).toFixed(scale)} {currency}
          </div>
        </>
      );
    }

    return (
      <div
        onMouseEnter={this.handlePopoverOpen}
        onMouseLeave={this.handlePopoverClose}
      >
        <Paper>
          <CardHeader>Earnings</CardHeader>
          <CardBody>{earningsInfo}</CardBody>
        </Paper>
        <Popover
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          id="mouse-over-popover"
          open={Boolean(anchorEl)}
          onClose={this.handlePopoverClose}
        >
          <Button simple color="info" justIcon onClick={this.props.getEarnings}>
            <Refresh />
          </Button>
        </Popover>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  earnings: state.earnings.earnings,
  status: state.earnings.status
});

export default connect(mapStateToProps, actions)(Earnings);
