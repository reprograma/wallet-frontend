import React, { Component } from 'react';
import { connect } from 'react-redux';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import * as actions from 'store/last-transactions/actions';

class LastTransactions extends Component {
  componentDidMount() {
    this.props.getLastTransactions();
  }

  render() {
    const { transactions, status } = this.props;

    if (status === 'pending' && !transactions.length) {
      return <div>Loading</div>;
    }

    const transactionsList = transactions.map(trans => {
      // snake_case from API response
      const {
        status,
        scale,
        amount,
        fee_info,
        type,
        total,
        updated,
        time_in,
        id
      } = trans;

      let totalAmount;
      let time;

      if (updated !== undefined) {
        time = new Date(updated);
      } else {
        time = new Date(time_in);
      }

      const timeDataOrig = new Date(time);
      timeDataOrig.setTime(
        timeDataOrig.getTime() - timeDataOrig.getTimezoneOffset() * 60 * 1000
      );

      const timeString = timeDataOrig.toUTCString().replace(' GMT', '');

      if (type === 'fee') {
        totalAmount =
          fee_info.amount / (10 ** fee_info.scale).toFixed(fee_info.scale);
      } else if (scale !== undefined) {
        totalAmount = (total / 10 ** scale).toFixed(scale);
      } else {
        totalAmount = amount;
      }

      return (
        <TableRow key={id}>
          <TableCell align="center">{status}</TableCell>
          <TableCell align="center">{totalAmount}</TableCell>
          <TableCell align="center">{timeString.substring(5)}</TableCell>
          <TableCell align="center">{type}</TableCell>
        </TableRow>
      );
    });

    return (
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell align="center">STATUS</TableCell>
              <TableCell align="center">AMOUNT</TableCell>
              <TableCell align="center">DATE</TableCell>
              <TableCell align="center">Service</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{transactionsList}</TableBody>
        </Table>
      </Paper>
    );
  }
}

const mapStateToProps = state => ({
  transactions: state.lastTransactions.transactions,
  status: state.lastTransactions.status
});

export default connect(mapStateToProps, actions)(LastTransactions);
