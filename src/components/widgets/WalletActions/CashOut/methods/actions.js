import { createAction } from 'redux-actions';
import { fetchApi } from 'utils';
import { CASH_OUT } from './constants';

const CASH_OUT_PATH = process.env.REACT_APP_CASH_OUT_PATH;

const reset = createAction(CASH_OUT, method => ({
  method,
  status: 'initial',
  error: null
}));

const begin = createAction(CASH_OUT, method => ({
  method,
  status: 'pending'
}));

const failure = createAction(CASH_OUT, (method, error) => ({
  method,
  error,
  status: 'error'
}));

const success = createAction(CASH_OUT, (method, response) => ({
  method,
  response,
  status: 'success'
}));

function cashOut(method, data) {
  return function cashOutThunk(dispatch) {
    return fetchApi({
      method: 'POST',
      data,
      path: `${CASH_OUT_PATH}/${method.cname}`
    })
      .then(response => dispatch(success(method, response.data)))
      .catch(error => {
        dispatch(failure(method, error.response.data.message));
      });
  };
}

const actions = {
  reset,
  begin,
  failure,
  success,
  cashOut
};

export default actions;
