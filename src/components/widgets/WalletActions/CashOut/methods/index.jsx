import React from 'react';
import Coin from './Coin';
import Sepa from './Sepa';
import Spark from './Spark';

const CashOutMethod = props => {
  const { method } = props;
  let res = <Coin {...props} />;
  if (method.cname === 'sepa') {
    res = <Sepa {...props} />;
  }
  if (method.cname === 'spark') {
    res = <Spark {...props} />;
  }
  return <div>{res}</div>;
};

export default CashOutMethod;
