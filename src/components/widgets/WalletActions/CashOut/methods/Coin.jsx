import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as transactionsActions from 'store/transactions/actions';
import * as walletActions from 'store/wallet/actions';
import Big from 'big.js';

import TextField from '@material-ui/core/TextField';
import Button from 'components/ui/Button';
import OneTimePin from 'components/ui/OneTimePin';

import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import SweetAlert from 'react-bootstrap-sweetalert';
import actions from './actions';

class Coin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: 0,
      address: '',
      concept: '',
      pin: '',
      snackbar: false,
      alert: null
    };
    this.handleCloseSnack = event => {
      this.setState({ snackbar: false });
    };
  }

  // TODO: fix
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.cash_out_error) {
      this.setState({
        loading: false,
        status: 'error',
        message: nextProps.cash_out_error,
        snackbar: true
      });
      nextProps.reset(this.props.method);
    }
    if (nextProps.cash_out_status === 'success') {
      this.setState({
        status: 'success',
        alert: (
          <SweetAlert
            success
            style={{ display: 'block', width: '30%', marginTop: '-100px' }}
            title="Success send!"
            onConfirm={() => this.hideAlert()}
            onCancel={() => this.hideAlert()}
          >
            {nextProps.cash_out_status.message}
          </SweetAlert>
        )
      });
      nextProps.reset(this.props.method);
      nextProps.getTransactions();
      nextProps.getWallet();
    }
  }

  hideAlert() {
    this.setState({ alert: null });
    this.props.handleClose();
  }

  handleSubmit() {
    const { amount, address, concept, pin } = this.state;
    const { method, company_id } = this.props;
    let scale_amount = amount * 1e8;
    if (method.cname !== 'fac') {
      scale_amount = Math.round(scale_amount);
    }
    const data = new FormData();
    data.set('amount', scale_amount);
    data.set('address', address);
    data.set('concept', concept);
    data.set('pin', pin);
    data.set('company_id', company_id);
    this.props.cashOut(method, data);
  }

  render() {
    const { fee, method, currency, tfa } = this.props;
    const { alert } = this.state;
    let FeeComponent = '';
    let CurrencyComponent = '';
    if (fee) {
      FeeComponent = (
        <div>
          <div style={{ display: 'flex' }}>
            <b>Fee</b>
          </div>
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <div>
              fixed: {fee.fixed / 10 ** fee.scale} {method.cname}
            </div>
            <div>variable: {fee.variable} %</div>
          </div>
        </div>
      );
    }
    if (currency) {
      CurrencyComponent = (
        <div>
          <b>Available</b>: {currency.available / 10 ** currency.scale}{' '}
          {method.cname}
          <div>
            <Button
              color="success"
              size="sm"
              onClick={() => {
                this.setState({
                  amount: new Big(
                    currency.available / 10 ** currency.scale
                  ).minus(
                    fee
                      ? fee.fixed / 10 ** fee.scale +
                          fee.variable * currency.available
                      : 0
                  )
                });
              }}
            >
              Send all my {method.cname}
            </Button>
          </div>
        </div>
      );
    }
    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'column'
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center'
          }}
        >
          <h1>
            {method.cname === 'fac' ? 'FAIR' : method.cname.toUpperCase()}
          </h1>
          {!tfa && <OneTimePin key={method.cname} />}
          <div>
            {CurrencyComponent}
            {FeeComponent}
          </div>
          <form
            onSubmit={event => {
              event.preventDefault();
              // TODO: fix
              // eslint-disable-next-line no-console
              console.log(`[debug] state: ${JSON.stringify(this.state)}`);
            }}
          >
            <div>
              <TextField
                label="amount"
                value={this.state.amount}
                onChange={event => {
                  this.setState({ amount: event.target.value });
                }}
                required
                type="number"
              />
              <i>
                {method.cname !== 'eth'
                  ? `with fees: ${new Big(
                      this.state.amount ? this.state.amount : 0
                    )
                      .plus(fee.fixed / 10 ** fee.scale)
                      .plus(fee.variable * this.state.amount)}`
                  : ''}
              </i>
            </div>
            <div>
              <TextField
                label="address"
                value={this.state.address}
                onChange={event => {
                  this.setState({ address: event.target.value });
                }}
                required
              />
            </div>
            <div>
              <TextField
                label="concept"
                value={this.state.concept}
                onChange={event => {
                  this.setState({ concept: event.target.value });
                }}
                required
              />
            </div>
            <div>
              <TextField
                label="pin"
                value={this.state.pin}
                onChange={event => {
                  this.setState({ pin: event.target.value });
                }}
                required
              />
            </div>
            <Button
              type="submit"
              onClick={() => this.handleSubmit()}
              color="success"
            >
              Send
            </Button>
          </form>
        </div>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center'
          }}
          open={this.state.snackbar}
          autoHideDuration={6000}
          onClose={this.handleCloseSnack}
          ContentProps={{
            'aria-describedby': 'message-id'
          }}
          message={<span id="message-id">{this.state.message}</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={this.handleCloseSnack}
            >
              <CloseIcon />
            </IconButton>
          ]}
        />
        {alert}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const { method } = ownProps;
  return {
    fee: state.walletActions.fees.find(
      f => f.service_name === `${method.cname}-out`
    ),
    currency: state.wallet.data.find(
      c => c.currency.toUpperCase() === method.cname.toUpperCase()
    ),
    cash_out_status: state.walletActions.cash_out.find(
      c => c.cname === method.cname
    ).send.status,
    cash_out_error: state.walletActions.cash_out.find(
      c => c.cname === method.cname
    ).send.error,
    tfa: state.user.data.two_factor_authentication,
    company_id: state.user.data.group_data.id
  };
};

const mapDispatchToProps = dispatch => {
  return {
    cashOut: actions.cashOut,
    reset: actions.reset,
    getTransactions: transactionsActions.getTransactions,
    getWallet: walletActions.getWallet
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(Coin);
