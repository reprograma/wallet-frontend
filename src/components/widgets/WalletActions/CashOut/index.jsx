import React, { Component } from 'react';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import Wizard from 'components/ui/Wizard';
import Transition from 'components/ui/Transition';

import SelectMethod from './SelectMethod';
import actions from './actions';
import Cash from './CashOut';

class CashOut extends Component {
  constructor() {
    super();
    this.state = {
      open: false
    };
    this.handleClose = () => {
      this.setState({ open: false });
    };
    this.handleOpen = () => {
      this.setState({ open: true });
    };
  }

  componentDidMount() {
    this.props.getFees();
  }

  render() {
    const { disabled } = this.props;
    return (
      <div>
        <Button onClick={this.handleOpen} disabled={disabled}>
          Cash Out
        </Button>
        <Dialog
          fullScreen
          onClose={this.handleClose}
          aria-labelledby="simple-dialog-title"
          open={this.state.open}
          TransitionComponent={Transition}
        >
          <div>
            <Toolbar>
              <IconButton
                color="inherit"
                onClick={this.handleClose}
                aria-label="Close"
              >
                <CloseIcon />
              </IconButton>
            </Toolbar>
            <Wizard
              steps={[
                {
                  stepName: 'Choice',
                  stepComponent: SelectMethod,
                  stepId: 'choice'
                },
                {
                  stepName: 'Cash',
                  stepComponent: props => (
                    <Cash {...props} close={() => this.handleClose()} />
                  ),
                  stepId: 'cash'
                }
              ]}
              validate
              title="Cash Out"
              subtitle=""
              finishButtonClick={this.handleClose}
            />
          </div>
        </Dialog>
      </div>
    );
  }
}
export default connect(() => ({}), actions)(CashOut);
