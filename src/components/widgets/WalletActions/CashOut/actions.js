import { createAction } from 'redux-actions';
import { fetchApi } from 'utils';
import { WALLET_ACTIONS_FEES } from './constants';

const WALLET_ACTIONS_FEES_PATH = process.env.REACT_APP_WALLET_ACTIONS_FEES_PATH;

const reset = createAction(WALLET_ACTIONS_FEES, () => ({
  status: 'initial'
}));

const begin = createAction(WALLET_ACTIONS_FEES, () => ({
  status: 'pending'
}));

const failure = createAction(WALLET_ACTIONS_FEES, error => ({
  error,
  status: 'error'
}));

const success = createAction(WALLET_ACTIONS_FEES, fees => ({
  fees,
  status: 'success'
}));

const getFees = () => dispatch => {
  dispatch(begin());
  return fetchApi({
    path: WALLET_ACTIONS_FEES_PATH
  })
    .then(response => dispatch(success(response.data)))
    .catch(error => dispatch(failure(error)));
};

const actions = {
  reset,
  begin,
  failure,
  success,
  getFees
};

export default actions;
