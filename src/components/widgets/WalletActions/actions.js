import { fetchApi } from 'utils';
import { WALLET_ACTIONS } from './constants';

const WALLET_ACTIONS_PATH = process.env.REACT_APP_WALLET_ACTIONS_PATH;

function reset() {
  return {
    type: WALLET_ACTIONS,
    payload: {
      status: 'initial'
    }
  };
}

function begin() {
  return {
    type: WALLET_ACTIONS,
    payload: {
      status: 'pending'
    }
  };
}

function failure(error) {
  return {
    type: WALLET_ACTIONS,
    payload: {
      status: 'error',
      error
    },
    error: true
  };
}

function success(methods) {
  return {
    type: WALLET_ACTIONS,
    payload: {
      methods,
      status: 'success'
    }
  };
}

function getActions() {
  return function getActionsThunk(dispatch) {
    dispatch(begin());
    return fetchApi({
      path: WALLET_ACTIONS_PATH
    })
      .then(response => dispatch(success(response.data)))
      .catch(error => dispatch(failure(error)));
  };
}

export { reset, begin, failure, success, getActions };
