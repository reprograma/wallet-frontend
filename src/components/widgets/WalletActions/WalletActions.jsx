import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from './actions';
import CashIn from './CashIn';
import CashOut from './CashOut';
import WalletToWallet from './WalletToWallet';

class WalletActions extends Component {
  componentDidMount() {
    this.props.getActions();
  }

  componentWillUnmount() {
    this.props.reset();
  }

  render() {
    return (
      <div>
        <div style={{ display: 'inline-block' }}>
          <CashIn
            disabled={
              this.props.status === 'pending' && !this.props.cashIn.length
            }
          />
        </div>
        <div style={{ display: 'inline-block' }}>
          <CashOut
            disabled={
              this.props.status === 'pending' && !this.props.cashOut.length
            }
          />
        </div>
        <div style={{ display: 'inline-block' }}>
          <WalletToWallet disabled={this.props.status === 'pending'} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  status: state.walletActions.status,
  cashIn: state.walletActions.cash_in,
  cashOut: state.walletActions.cash_out
});

export default connect(mapStateToProps, actions)(WalletActions);
