import { WALLET_ACTIONS } from './constants';
import {
  CASH_IN_TOKENS,
  CASH_IN_TOKEN_CREATE,
  CASH_IN_TOKEN_REMOVE
} from './CashIn/methods/constants';
import { WALLET_ACTIONS_FEES } from './CashOut/constants';
import { WALLET_TO_WALLET } from './WalletToWallet/constants';
import { CASH_OUT } from './CashOut/methods/constants';

// TODO: correct snake_case to camelCase
const initialState = {
  status: 'initial',
  cash_in: [],
  cash_out: [],
  fees: [],
  fees_status: 'initial',
  error: null,
  methods: [],
  walletToWallet: {
    status: 'initial',
    response: null,
    error: null
  }
};

function walletActionsReducer(state = initialState, action) {
  switch (action.type) {
    case WALLET_ACTIONS: {
      const { error = state.error, status = state.status } = action.payload;

      let { methods = state.methods } = action.payload;

      let cash_in = state.cash_in;
      let cash_out = state.cash_out;

      if (methods) {
        let sw = false;
        let sw2 = false;
        methods = methods.filter(method => {
          let result = false;
          if (method.cname === 'halcash_es') {
            sw = true;
          }
          if (method.cname === 'sepa' && method.type === 'out') {
            sw2 = true;
          }
          if (
            ((method.cname === 'halcash_pl' && sw === false) ||
              method.cname !== 'halcash_pl') &&
            ((method.cname === 'transfer' && sw2 === false) ||
              method.cname !== 'transfer') &&
            method.cname !== 'echo' &&
            method.cname !== 'teleingreso_pt' &&
            method.cname !== 'teleingreso_usa' &&
            method.cname !== 'paynet_reference'
          ) {
            result = true;
          }
          return result;
        });
        cash_out = methods
          .filter(method => method.type === 'out')
          .map(method => ({ ...method, send: { status: '', error: null } }));
        cash_in = methods
          .filter(method => method.type === 'in')
          .map(method => ({ ...method, tokens: { status: '', list: [] } }));
      }
      return {
        ...state,
        cash_in,
        cash_out,
        error,
        status
      };
    }

    case CASH_IN_TOKENS: {
      const { method, status, error, list } = action.payload;
      const cash_in_method = state.cash_in.find(m => m.cname === method.cname);
      const indexOfMethod = state.cash_in.findIndex(
        m => m.cname === method.cname
      );
      const cash_in = state.cash_in;
      let tokens = {};
      if (list) {
        tokens = {
          status,
          error,
          list
        };
      } else {
        tokens = {
          status,
          list: cash_in_method.tokens.list
        };
      }
      cash_in_method.tokens = tokens;
      cash_in[indexOfMethod] = cash_in_method;

      return {
        ...state,
        cash_in,
        error: error || state.error
      };
    }
    case CASH_IN_TOKEN_CREATE: {
      const { method, status, error, token } = action.payload;
      const cash_in_method = state.cash_in.find(m => m.cname === method.cname);
      const indexOfMethod = state.cash_in.findIndex(
        m => m.cname === method.cname
      );
      const cash_in = state.cash_in;
      const elements = cash_in_method.tokens.list.elements;
      if (token) {
        elements.push(token);
      }
      cash_in_method.tokens.list.elements = elements;
      cash_in_method.tokens.status = status;
      cash_in[indexOfMethod] = cash_in_method;
      return {
        ...state,
        cash_in,
        error: error || state.error
      };
    }
    case CASH_IN_TOKEN_REMOVE: {
      const { method, status, error, id } = action.payload;
      const cash_in_method = state.cash_in.find(m => m.cname === method.cname);
      const indexOfMethod = state.cash_in.findIndex(
        m => m.cname === method.cname
      );
      const cash_in = state.cash_in;
      let elements = cash_in_method.tokens.list.elements;
      if (id) {
        elements = elements.filter(element => element.id !== id);
      }
      cash_in_method.tokens.list.elements = elements;
      cash_in_method.tokens.status = status;
      cash_in[indexOfMethod] = cash_in_method;
      return {
        ...state,
        cash_in,
        error: error || state.error
      };
    }
    case CASH_OUT: {
      const { method, status, error, response } = action.payload;
      const cash_out_method = state.cash_out.find(
        m => m.cname === method.cname
      );
      const indexOfMethod = state.cash_out.findIndex(
        m => m.cname === method.cname
      );
      const cash_out = state.cash_out;
      const send = { status, error, response };
      cash_out_method.send = send;
      cash_out[indexOfMethod] = cash_out_method;

      return {
        ...state,
        cash_out,
        error: error || state.error
      };
    }
    case WALLET_ACTIONS_FEES: {
      const {
        fees = state.fees,
        status = state.fees_status,
        error
      } = action.payload;

      return {
        ...state,
        fees,
        fees_status: status,
        error: error || state.error
      };
    }
    case WALLET_TO_WALLET: {
      const {
        status = state.walletToWallet.status,
        response = state.walletToWallet.response,
        error = state.walletToWallet.error
      } = action.payload;

      return {
        ...state,
        walletToWallet: {
          ...state.walletToWallet,
          response,
          status,
          error
        }
      };
    }
    default:
      return state;
  }
}

export { walletActionsReducer };
