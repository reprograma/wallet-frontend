import { createAction } from 'redux-actions';
import { fetchApi } from 'utils';
import { WALLET_TO_WALLET } from './constants';

const WALLET_TO_WALLET_PATH = process.env.REACT_APP_WALLET_TO_WALLET_PATH;

const reset = createAction(WALLET_TO_WALLET, () => ({
  status: 'initial'
}));

const begin = createAction(WALLET_TO_WALLET, () => ({
  status: 'pending'
}));

const failure = createAction(WALLET_TO_WALLET, error => ({
  error,
  status: 'error'
}));

const success = createAction(WALLET_TO_WALLET, response => ({
  response,
  status: 'success'
}));

const sendToWallet = params => dispatch => {
  dispatch(begin);
  const { token, pin, currency } = params;
  let { concept, amount } = params;
  concept = concept.replace(/[^a-zA-Z 0-9.]+/g, ' ');
  let scale = 2;
  if (
    currency === 'BTC' ||
    currency === 'FAC' ||
    currency === 'ETH' ||
    currency === 'CREA' ||
    currency === 'FAIRP'
  ) {
    scale = 8;
  }
  amount *= (10 ** scale).toFixed(scale);
  const data = new FormData();
  data.set('token', token);
  data.set('pin', pin);
  data.set('amount', amount);
  data.set('concept', concept);
  fetchApi({
    path: `${WALLET_TO_WALLET_PATH}/${currency}`,
    method: 'POST',
    body: data
  })
    .then(response => {
      dispatch(success(response));
    })
    .catch(error => {
      dispatch(failure(error.response.data));
    });
};

const actions = {
  reset,
  begin,
  failure,
  success,
  sendToWallet
};

export default actions;
