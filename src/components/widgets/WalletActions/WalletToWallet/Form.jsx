import React, { useState, useCallback } from 'react';
import { TextField, Button, Select } from '@material-ui/core';
import { connect } from 'react-redux';
import actions from './actions';

const currencies = [
  {
    name: 'FAIR',
    value: 'FAC'
  },
  {
    name: 'BTC',
    value: 'BTC'
  },
  {
    name: 'ETH',
    value: 'ETH'
  },
  {
    name: 'EUR',
    value: 'EUR'
  }
];

const WalletForm = props => {
  const [amount, setAmount] = useState('');
  const [concept, setConcept] = useState('');
  const [accToken, setAccToken] = useState('');
  const [pin, setPin] = useState('');
  const [currency, setCurrency] = useState('FAC');

  const { sendToWallet } = props;
  const handleSubmit = useCallback(
    event => {
      event.preventDefault();
      sendToWallet({ amount, concept, token: accToken, pin, currency });
    },
    [amount, concept, currency, accToken, pin, sendToWallet]
  );
  return (
    <form style={{ display: 'flex', flexDirection: 'column' }}>
      <Select
        native
        value={currency.name}
        onChange={event => setCurrency(event.target.value)}
        inputProps={{
          name: 'age',
          id: 'age-native-simple'
        }}
      >
        {currencies.map(curr => (
          <option key={curr.name} value={curr.value}>
            {curr.name}
          </option>
        ))}
      </Select>
      <TextField
        label="Amount"
        value={amount}
        onChange={event => setAmount(event.target.value)}
        type="number"
        required
      />
      <TextField
        label="Concept"
        value={concept}
        onChange={event => setConcept(event.target.value)}
        required
      />
      <TextField
        label="Account token"
        value={accToken}
        onChange={event => setAccToken(event.target.value)}
        required
      />
      <TextField
        value={pin}
        onChange={event => setPin(event.target.value)}
        label="Pin (optional)"
      />
      <Button type="submit" onClick={handleSubmit}>
        Continue
      </Button>
    </form>
  );
};

export default connect(() => {}, actions)(WalletForm);
