import React, { useState, useCallback, useEffect } from 'react';
import { connect } from 'react-redux';

import { Button, Dialog, Snackbar, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import OneTimePin from 'components/ui/OneTimePin';

import { getWallet } from 'store/wallet/actions';
import { getTransactions } from 'store/transactions/actions';
import Form from './Form';

const WalletToWallet = props => {
  const { disabled, tfa, status, error, getWallet, getTransactions } = props;
  const message = error ? error.message : '';
  const [openDialog, setOpenDialog] = useState(false);
  const [snackbar, handleChangeSnackBar] = useState(false);

  const fullScreen = useMediaQuery('(max-width:480px)');

  const handleChangeDialog = useCallback(() => {
    setOpenDialog(!openDialog);
  }, [openDialog]);

  useEffect(() => {
    if (status === 'success') {
      handleChangeSnackBar('Success Send to wallet');
      getWallet();
      getTransactions();
      handleChangeDialog();
    } else if (status === 'error') {
      handleChangeSnackBar(`Send failed: ${message}`);
    }
  }, [status, message, error, getWallet, getTransactions, handleChangeDialog]);

  return (
    <div>
      <Button disabled={disabled} onClick={handleChangeDialog}>
        Wallet To Wallet{' '}
      </Button>
      <Dialog
        fullScreen={fullScreen}
        open={openDialog}
        style={{ alignItems: 'center' }}
      >
        <div
          style={{
            padding: 20,
            minWidth: 400,
            justifyContent: 'space-between',
            display: 'flex',
            width: '100%'
          }}
        >
          <h4>Wallet To Wallet</h4>
          <Button key="close" aria-label="Close" onClick={handleChangeDialog}>
            <CloseIcon />
          </Button>
        </div>
        <hr />
        <div
          style={{
            display: 'flex',
            width: '100%',
            padding: 40,
            flexDirection: 'column',
            justifyContent: 'center'
          }}
        >
          {!tfa && <OneTimePin key="WalletToWallet" />}
          <div>
            Send money from your wallet to another company wallet inside
            BankOfTheCommons
          </div>
          <div>
            <Form />
          </div>
        </div>
      </Dialog>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center'
        }}
        open={Boolean(snackbar)}
        autoHideDuration={6000}
        onClose={() => handleChangeSnackBar(false)}
        ContentProps={{
          'aria-describedby': 'message-id'
        }}
        message={<span id="message-id">{snackbar}</span>}
        action={[
          <IconButton
            key="close"
            aria-label="Close"
            color="inherit"
            onClose={() => handleChangeSnackBar(false)}
          >
            <CloseIcon />
          </IconButton>
        ]}
      />
    </div>
  );
};

const mapStateToProps = state => {
  let tfa;
  let company_id;

  if (state.user.data) {
    tfa = state.user.data.two_factor_authentication;
    company_id = state.user.data.group_data
      ? state.user.data.group_data.id
      : null;
  } else {
    tfa = null;
    company_id = null;
  }

  return {
    tfa,
    company_id,
    status: state.walletActions.walletToWallet.status,
    error: state.walletActions.walletToWallet.error,
    data: state.walletActions.walletToWallet.data
  };
};

export default connect(mapStateToProps, { getWallet, getTransactions })(
  WalletToWallet
);
