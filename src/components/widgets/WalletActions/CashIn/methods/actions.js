import axios from 'axios';
import { createAction } from 'redux-actions';
import { getAuthToken } from 'utils';
import {
  CASH_IN_TOKENS,
  CASH_IN_TOKEN_CREATE,
  CASH_IN_TOKEN_REMOVE
} from './constants';

const apiBaseURL = `${process.env.REACT_APP_BACKEND_URL}/${process.env.REACT_APP_COMPANY_WALLET_PATH}`;

const reset = createAction(CASH_IN_TOKENS, method => ({
  method,
  status: 'initial'
}));

const begin = createAction(CASH_IN_TOKENS, method => ({
  method,
  status: 'pending'
}));

const failure = createAction(CASH_IN_TOKENS, (method, error) => ({
  method,
  error,
  status: 'error'
}));

const success = createAction(CASH_IN_TOKENS, (method, list) => ({
  method,
  list,
  status: 'success'
}));

const create = createAction(CASH_IN_TOKEN_CREATE, (method, token) => ({
  method,
  token,
  status: 'created'
}));

const remove = createAction(CASH_IN_TOKEN_REMOVE, (method, id) => ({
  method,
  id,
  status: 'removed'
}));

const createCashInToken = (method, label) => dispatch => {
  const body = new FormData();
  body.set('method', `${method.cname}-in`);
  body.set('label', label);
  axios({
    method: 'POST',
    data: body,
    headers: {
      Authorization: `Bearer ${getAuthToken()}`
    },
    url: `${apiBaseURL}/cash_in_token`
  })
    .then(response => dispatch(create(method, response.data)))
    .catch(error => dispatch(failure(error)));
};

const removeCashInToken = (method, id) => dispatch => {
  const body = new FormData();
  body.set('disable', true);
  axios({
    method: 'PUT',
    data: 'disable=true',
    headers: {
      Authorization: `Bearer ${getAuthToken()}`,
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    },
    url: `${apiBaseURL}/cash_in_token/${id}?`
  })
    // TODO: fix me
    .then(() => {
      dispatch(remove(method, id));
      // getCashInList(method);
    })
    .catch(error => dispatch(failure(error)));
};

const getCashInList = method => dispatch => {
  dispatch(begin(method));
  axios({
    method: 'GET',
    headers: {
      Authorization: `Bearer ${getAuthToken()}`
    },
    url: `${apiBaseURL}/cash_in_token/${method.cname}-in`
  })
    .then(response => dispatch(success(method, response.data)))
    .catch(error => dispatch(failure(error)));
};

export {
  reset,
  begin,
  failure,
  success,
  getCashInList,
  createCashInToken,
  removeCashInToken
};
