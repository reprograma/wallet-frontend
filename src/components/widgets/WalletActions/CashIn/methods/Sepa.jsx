import React, { Component } from 'react';

import Button from 'components/ui/Button';

class Sepa extends Component {
  createCashInToken() {
    this.props.createCashInToken(this.props.method, 'default');
  }

  render() {
    const { list, status } = this.props;
    let bank_info;
    let element;

    if (list.elements) {
      element = list.elements[0];
    }
    if (list.bank_info) {
      bank_info = list.bank_info[0];
    }
    if (status === 'pending') {
      return <div />;
    }
    if (!(bank_info && element)) {
      return (
        <div>
          Please generate your reference.
          <Button onClick={() => this.createCashInToken()} color="success">
            {' '}
            Generate Reference{' '}
          </Button>
        </div>
      );
    }
    return (
      <div>
        <p>
          In order to deposit <b>EUR</b> into our BotC account you must send a
          bank transfer to one of the following account&apos;s details:
        </p>
        <div>
          <div>
            <p>
              <b>BENEFICIARY:</b>
              {bank_info.beneficiary}
            </p>
            <p>
              <b>IBAN:</b> {bank_info.iban}{' '}
            </p>
            <p>
              <b>BIC/SWIFT:</b> {bank_info.bic_swift}{' '}
            </p>
            <p>
              <b>BANK:</b> {bank_info.bank}{' '}
            </p>
            <p>
              <b>REFERENCE:</b> {element.token}{' '}
            </p>
            <p>
              <b>IMPORTANT:</b>
            </p>
            <p>
              <b>* ENSURE</b> the reference <b>MATCHES EXACTLY</b> with the
              given above, otherwise you will need to contact us to make it
              manually - which will take more time.{' '}
            </p>
            <p>EXTRA NOTES:</p>

            <p>
              * The transfer can take up to two-three working days to appear in
              your wallet (from the moment the transfer was done).{' '}
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default Sepa;
