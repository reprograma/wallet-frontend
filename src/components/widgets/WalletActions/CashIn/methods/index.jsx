import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from './actions';
import Coin from './Coin';
import Sepa from './Sepa';

class CashInMethod extends Component {
  constructor() {
    super();
    this.state = {
      // TODO: fix me
      // eslint-disable-next-line react/no-unused-state
      loading: true
    };
  }

  componentDidMount() {
    this.props.getCashInList(this.props.method);
  }

  // TODO: fix me
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.status === 'success') {
      // TODO: fix me
      // eslint-disable-next-line react/no-unused-state
      this.setState({ loading: false });
    }
    if (nextProps.method !== this.props.method) {
      this.props.getCashInList(nextProps.method);
    }
    if (nextProps.status === 'created' || nextProps.status === 'removed') {
      this.props.reset(nextProps.method);
    }
  }

  removeToken(id) {
    this.props.removeCashInToken(this.props.method, id);
  }

  createCashInToken() {
    this.props.createCashInToken(this.props.method, this.state.label);
    this.setState({ label: '' });
  }

  render() {
    const { method } = this.props;
    let res = <Coin {...this.props} />;
    if (['sepa', 'easypay'].indexOf(method.cname) > -1) {
      res = <Sepa {...this.props} />;
    }
    return res;
  }
}

const mapStateToProps = (state, ownProps) => {
  const method = state.walletActions.cash_in.find(
    m => ownProps.method.cname === m.cname
  );
  return {
    list: method.tokens.list,
    status: method.tokens.status
  };
};

export default connect(mapStateToProps, actions)(CashInMethod);
