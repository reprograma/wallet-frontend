import React, { Component } from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import Wizard from 'components/ui/Wizard';
import Transition from 'components/ui/Transition';

import SelectMethod from './SelectMethod';
import Cash from './Cash';

// interface Drawer {
//   handleClose: () => void;
//   handleOpen: () => void;
// }

class CashIn extends Component {
  constructor() {
    super();
    this.handleClose = this.handleClose.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
  }

  state = {
    open: false
  };

  handleClose() {
    this.setState({ open: false });
  }

  handleOpen() {
    this.setState({ open: true });
  }

  render() {
    const { disabled } = this.props;
    return (
      <div>
        <Button onClick={this.handleOpen} disabled={disabled}>
          Cash In
        </Button>
        <Dialog
          fullScreen
          onClose={this.handleClose}
          aria-labelledby="simple-dialog-title"
          open={this.state.open}
          TransitionComponent={Transition}
        >
          <div>
            <Toolbar>
              <IconButton
                color="inherit"
                onClick={this.handleClose}
                aria-label="Close"
              >
                <CloseIcon />
              </IconButton>
            </Toolbar>
            <Wizard
              steps={[
                {
                  stepName: 'Choice',
                  stepComponent: SelectMethod,
                  stepId: 'choice'
                },
                {
                  stepName: 'Cash',
                  stepComponent: Cash,
                  stepId: 'cash'
                }
              ]}
              validate
              title="Cash In"
              subtitle=""
              finishButtonClick={this.handleClose}
            />
          </div>
        </Dialog>
      </div>
    );
  }
}
export default CashIn;
