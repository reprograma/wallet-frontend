import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  }
});

class SelectMethod extends React.Component {
  state = {
    selectedMethod: ''
  };

  handleListItemClick = (event, method) => {
    this.setState({ selectedMethod: method });
  };

  sendState() {
    return this.state;
  }

  isValidated() {
    return this.state.selectedMethod !== '';
  }

  render() {
    const { methods } = this.props;
    const items = methods.map(method => {
      return (
        <ListItem
          button
          disabled={method.status === 'unavailable'}
          selected={this.state.selectedMethod === method}
          onClick={event => this.handleListItemClick(event, method)}
          key={method.name}
        >
          <img height={40} width={120} alt="" src={method.image} />
        </ListItem>
      );
    });
    return (
      <Grid container justify="center">
        <List component="nav" justify="center">
          {items}
        </List>
      </Grid>
    );
  }
}

const mapStateToProps = state => ({
  methods: state.walletActions.cash_in
});

export default connect(mapStateToProps, {})(withStyles(styles)(SelectMethod));
