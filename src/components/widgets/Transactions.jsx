import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactTable from 'react-table';

// @material-ui/icons
import Dvr from '@material-ui/icons/Dvr';
import Favorite from '@material-ui/icons/Favorite';
import Close from '@material-ui/icons/Close';

import Paper from '@material-ui/core/Paper';

// core components
import Button from 'components/ui/Button';

import * as actions from 'store/transactions/actions';

class Transactions extends Component {
  componentDidMount() {
    this.props.getTransactions();
  }

  render() {
    const { transactions, status } = this.props;

    if (status === 'pending' && !transactions.length) {
      return <div>Loading</div>;
    }

    const transactionsList = transactions.map(trans => {
      // snake_case from API response
      const {
        status,
        scale,
        amount,
        fee_info,
        type,
        total,
        updated,
        time_in,
        id
      } = trans;

      let totalAmount;
      let time;

      if (updated !== undefined) {
        time = new Date(updated);
      } else {
        time = new Date(time_in);
      }

      const timeDataOrig = new Date(time);
      timeDataOrig.setTime(
        timeDataOrig.getTime() - timeDataOrig.getTimezoneOffset() * 60 * 1000
      );

      const timeString = timeDataOrig.toUTCString().replace(' GMT', '');

      if (type === 'fee') {
        totalAmount =
          fee_info.amount / (10 ** fee_info.scale).toFixed(fee_info.scale);
      } else if (scale !== undefined) {
        totalAmount = (total / 10 ** scale).toFixed(scale);
      } else {
        totalAmount = amount;
      }

      // TODO: fix alerts
      return {
        id,
        status,
        amount: totalAmount,
        date: timeString.substring(5),
        service: type,
        actions: (
          // we've added some custom button actions
          <div className="actions-right">
            {/* use this button to add a like kind of action */}
            <Button
              justIcon
              round
              simple
              onClick={() =>
                // eslint-disable-next-line no-alert
                alert("You've pressed the like button on column id: ")
              }
              color="info"
              className="like"
            >
              <Favorite />
            </Button>{' '}
            {/* use this button to add a edit kind of action */}
            <Button
              justIcon
              round
              simple
              onClick={() =>
                // eslint-disable-next-line no-alert
                alert("You've pressed the edit button on colmun id: ")
              }
              color="warning"
              customclass="edit"
            >
              <Dvr />
            </Button>{' '}
            {/* use this button to remove the data row */}
            <Button
              justIcon
              round
              simple
              onClick={() =>
                // eslint-disable-next-line no-alert
                alert("You've pressed the delete button on colmun id: ")
              }
              color="danger"
              customclass="remove"
            >
              <Close />
            </Button>{' '}
          </div>
        )
      };
    });

    return (
      <Paper>
        <ReactTable
          data={transactionsList}
          filterable
          columns={[
            {
              Header: 'Status',
              accessor: 'status'
            },
            {
              Header: 'Amount',
              accessor: 'amount'
            },
            {
              Header: 'Date',
              accessor: 'date'
            },
            {
              Header: 'Service',
              accessor: 'service'
            },
            {
              Header: 'Actions',
              accessor: 'actions',
              sortable: false,
              filterable: false
            }
          ]}
          defaultPageSize={10}
          showPaginationTop
          showPaginationBottom={false}
          className="-striped -highlight"
        />
      </Paper>
    );
  }
}

const mapStateToProps = state => ({
  transactions: state.transactions.transactions,
  status: state.transactions.status
});

export default connect(mapStateToProps, actions)(Transactions);
