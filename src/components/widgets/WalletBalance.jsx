import React, { Component } from 'react';
import { connect } from 'react-redux';

import GridContainer from 'components/ui/Grid/GridContainer';
import GridItem from 'components/ui/Grid/GridItem';
import Card from 'components/ui/Card/Card';
import CardHeader from 'components/ui/Card/CardHeader';
import CardBody from 'components/ui/Card/CardBody';

// TODO: fix
/* eslint-disable react/prefer-stateless-function */

class WalletBalance extends Component {
  render() {
    const { balances } = this.props;

    let balancesList;

    if (balances.length > 0) {
      balancesList = balances
        .filter(balance => balance.status === 'enabled')
        .map(balance => {
          const { id, currency, available, scale } = balance;
          return (
            <GridItem key={id}>
              <CardBody>
                {currency}
                <br />
                {available / (10 ** scale).toFixed(3)}
              </CardBody>
            </GridItem>
          );
        });
    }

    let totalBalance;

    if (balances.length > 0) {
      const { id, currency, available, scale } = balances[balances.length - 1];
      totalBalance = (
        <GridItem key={id}>
          <CardBody>
            Total balance {currency}
            <br />
            {available / (10 ** scale).toFixed(3)}
          </CardBody>
        </GridItem>
      );
    }

    return (
      <div>
        <Card>
          <CardHeader>Wallet</CardHeader>
          <GridContainer>
            {totalBalance}
            {balancesList}
          </GridContainer>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  balances: state.wallet.wallet
});

export default connect(mapStateToProps, {})(WalletBalance);
