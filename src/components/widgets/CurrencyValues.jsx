import React, { Component } from 'react';
import { connect } from 'react-redux';

import GridContainer from 'components/ui/Grid/GridContainer';
import GridItem from 'components/ui/Grid/GridItem';
import Card from 'components/ui/Card/Card';
import CardHeader from 'components/ui/Card/CardHeader';
import CardBody from 'components/ui/Card/CardBody';

import * as actions from 'store/currency-values/actions';

class CurrencyValues extends Component {
  componentDidMount() {
    this.props.getValues();
  }

  render() {
    const { values = {}, status } = this.props;
    if (status === 'pending' && !Object.keys(values).length) {
      return 'Loading';
    }
    const valuesList = Object.keys(values).map(key => {
      return (
        <GridItem key={key}>
          <CardBody>
            {key}
            <br />
            {values[key]}
          </CardBody>
        </GridItem>
      );
    });

    return (
      <div>
        <Card>
          <CardHeader>Currency values</CardHeader>
          <GridContainer>{valuesList}</GridContainer>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  values: state.currencyValues.values,
  status: state.currencyValues.status
});

export default connect(mapStateToProps, actions)(CurrencyValues);
