import React from 'react';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch
} from 'react-router-dom';

import LoginLayout from 'components/layouts/LoginLayout';
import DashboardLayout from 'components/layouts/DashboardLayout';

import { isLoggedIn } from 'utils';

import PrivateRoute from './PrivateRoute';

const MainRoutes = () => (
  <Router>
    <Switch>
      <Route exact path="/login" component={LoginLayout} />
      <PrivateRoute
        path="/dashboard"
        authPath="/login"
        component={DashboardLayout}
        isAuthorized={isLoggedIn}
      />
      <Redirect to="/dashboard" />
    </Switch>
  </Router>
);

export default MainRoutes;
