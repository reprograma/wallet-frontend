import Dashboard from 'components/pages/Dashboard';
import Wallet from 'components/pages/Wallet';

// @material-ui/icons
import DashboardIcon from '@material-ui/icons/Dashboard';
import WalletIcon from '@material-ui/icons/AccountBalanceWallet';

const dashboardRoutes = [
  {
    id: 0,
    path: '/dashboard/metrics',
    name: 'Dashboard',
    icon: DashboardIcon,
    component: Dashboard
  },
  {
    id: 1,
    path: '/dashboard/wallet',
    name: 'Wallet',
    icon: WalletIcon,
    component: Wallet
  },
  {
    id: 2,
    redirect: true,
    path: '/dashboard',
    pathTo: '/dashboard/metrics',
    name: 'Dashboard Redirect'
  }
];

export default dashboardRoutes;
