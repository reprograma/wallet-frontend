import React from 'react';
import { Route, Redirect } from 'react-router-dom';

function PrivateRoute({
  isAuthorized = () => false,
  authPath = '/auth',
  ...props
}) {
  if (!isAuthorized()) {
    return <Redirect to={authPath} />;
  }

  return <Route {...props} />;
}

export default PrivateRoute;
