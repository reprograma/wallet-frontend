import React from 'react';

import WalletBalance from 'components/widgets/WalletBalance';
import Transactions from 'components/widgets/Transactions';
import WalletActions from 'components/widgets/WalletActions';

const Wallet = () => {
  return (
    <div>
      <WalletBalance />
      <WalletActions />
      <Transactions />
    </div>
  );
};

export default Wallet;
