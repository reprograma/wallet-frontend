import React from 'react';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles';

import dashboardStyle from 'assets/jss/material-dashboard-react/views/dashboardStyle';
import GridContainer from 'components/ui/Grid/GridContainer';
import GridItem from 'components/ui/Grid/GridItem';

// widgets
import CurrencyValues from 'components/widgets/CurrencyValues';
import Earnings from 'components/widgets/Earnings';
import MonthEarnings from 'components/widgets/MonthEarnings';
import LastTransactions from 'components/widgets/LastTransactions';

const Dashboard = () => (
  <div>
    <CurrencyValues />
    <MonthEarnings />
    <GridContainer
      direction="row"
      justify="space-between"
      alignItems="flex-start"
      container
    >
      <GridItem xs={7}>
        <LastTransactions />
      </GridItem>
      <GridItem xs={5}>
        <Earnings />
      </GridItem>
    </GridContainer>
  </div>
);

export default withStyles(dashboardStyle)(Dashboard);
