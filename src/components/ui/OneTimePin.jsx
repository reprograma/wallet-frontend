import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import CheckIcon from '@material-ui/icons/Check';
import Fab from '@material-ui/core/Fab';
import { fetchApi } from 'utils';

const ONE_TIME_PIN_PATH = process.env.REACT_APP_ONE_TIME_PIN_PATH;

const sendOneTimePin = () => {
  return fetchApi({
    method: 'PUT',
    path: ONE_TIME_PIN_PATH
  })
    .then(response => response.data)
    .catch(error => {
      throw error;
    });
};

class OneTimePin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      success: false
    };
  }

  handleButtonClick = () => {
    if (!this.state.loading) {
      this.setState({ success: false, loading: true });
      sendOneTimePin()
        .then(() => {
          this.setState({ success: true, loading: false });
        })
        .catch(() => {
          this.setState({ success: false, loading: false });
        });
    }
  };

  render() {
    const { loading, success } = this.state;
    const Progress = loading ? (
      <CircularProgress
        style={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          marginTop: -12,
          marginLeft: -12
        }}
        size={24}
      />
    ) : (
      <div />
    );
    const SendButton = (
      <div>
        <Button
          variant="contained"
          style={{ backgroundColor: 'green' }}
          disabled={loading}
          onClick={this.handleButtonClick}
        >
          Send me one-time pin
        </Button>
        ,{Progress}
      </div>
    );
    const SuccessBadge = (
      <Fab size="small" style={{ backgroundColor: 'green' }}>
        <CheckIcon />
      </Fab>
    );
    return (
      <div style={{ width: '100%', textAlign: 'center', marginBottom: '12px' }}>
        This transaction requires a confirmation. You don&apos;t have 2FA
        enabled. Please request a one-time PIN for your withdrawal with the link
        below. We will send you an email with the PIN.
        <div style={{ alignText: 'right', position: 'relative' }}>
          {!success ? SendButton : SuccessBadge}
        </div>
      </div>
    );
  }
}

export default OneTimePin;
