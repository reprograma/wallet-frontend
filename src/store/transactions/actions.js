import { fetchApi } from 'utils';
import { TRANSACTIONS } from './constants';

const TRANSACTIONS_PATH = process.env.REACT_APP_TRANSACTIONS_PATH;

function reset() {
  return {
    type: TRANSACTIONS,
    payload: {
      status: 'initial'
    }
  };
}

function begin() {
  return {
    type: TRANSACTIONS,
    payload: {
      status: 'pending'
    }
  };
}

function failure(error) {
  return {
    type: TRANSACTIONS,
    payload: {
      status: 'error',
      error
    },
    error: true
  };
}

function success(transactions) {
  return {
    type: TRANSACTIONS,
    payload: {
      status: 'success',
      transactions
    }
  };
}

function getTransactions() {
  return function(dispatch) {
    dispatch(begin());
    return fetchApi({ path: TRANSACTIONS_PATH })
      .then(response => dispatch(success(response.data.elements)))
      .catch(error => dispatch(failure(error)));
  };
}

export { reset, begin, failure, success, getTransactions };
