import { TRANSACTIONS } from './constants';

const initialState = {
  status: 'initial',
  transactions: [],
  error: null
};

function transactionsReducer(state = initialState, action) {
  if (action.type === TRANSACTIONS) {
    const { status, transactions, error } = action.payload;

    return {
      ...state,
      status,
      transactions: transactions || state.transactions,
      error: error || state.error
    };
  }

  return state;
}

export { transactionsReducer };
