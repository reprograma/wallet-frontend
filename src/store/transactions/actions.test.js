import { TRANSACTIONS } from './constants';

import { reset, begin, success, failure, getTransactions } from './actions';

test('action: reset', () => {
  const action = reset();
  expect(action.type).toEqual(TRANSACTIONS);
  expect(action.payload.status).toEqual('initial');
});

test('action: begin', () => {
  const action = begin();
  expect(action.type).toEqual(TRANSACTIONS);
  expect(action.payload.status).toEqual('pending');
});

test('action: success', () => {
  const wallet = {};
  const action = success(wallet);
  expect(action.type).toEqual(TRANSACTIONS);
  expect(action.payload.status).toEqual('success');
  expect(action.payload.transactions).toEqual(wallet);
});

test('action: failure', () => {
  const error = new Error();
  const action = failure(error);
  expect(action.type).toEqual(TRANSACTIONS);
  expect(action.payload.status).toEqual('error');
  expect(action.error).toBe(true);
  expect(action.payload.error).toEqual(error);
});

test('action (thunk): getTransactions - success', async () => {
  const mockDispatch = jest.fn();

  function mockFetch(data) {
    return jest.fn().mockImplementation(() =>
      Promise.resolve({
        ok: true,
        json: () => data
      })
    );
  }

  const response = {
    data: { elements: [{}] }
  };

  global.fetch = mockFetch(response);

  await getTransactions()(mockDispatch);

  const actionBegin = mockDispatch.mock.calls[0][0];
  const actionSuccess = mockDispatch.mock.calls[1][0];

  expect(mockDispatch.mock.calls.length).toBe(2);
  expect(actionBegin.type).toEqual(TRANSACTIONS);
  expect(actionSuccess.type).toEqual(TRANSACTIONS);
  expect(actionSuccess.payload.transactions).toEqual(response.data.elements);
});

test('action (thunk): getTransactions - failure', async () => {
  const mockDispatch = jest.fn();

  const error = new Error();
  global.fetch = () => Promise.reject(error);

  await getTransactions()(mockDispatch);

  const actionBegin = mockDispatch.mock.calls[0][0];
  const actionFailure = mockDispatch.mock.calls[1][0];

  expect(actionBegin.type).toEqual(TRANSACTIONS);
  expect(actionFailure.type).toEqual(TRANSACTIONS);
  expect(actionFailure.error).toBe(true);
  expect(actionFailure.payload.error).toEqual(error);
});
