import { fetchApi } from 'utils';
import { EARNINGS } from './constants';

const EARNINGS_PATH = process.env.REACT_APP_EARNINGS_PATH;

function reset() {
  return {
    type: EARNINGS,
    payload: {
      status: 'initial'
    }
  };
}

function begin() {
  return {
    type: EARNINGS,
    payload: {
      status: 'pending'
    }
  };
}

function failure(error) {
  return {
    type: EARNINGS,
    payload: {
      status: 'error',
      error
    },
    error: true
  };
}

function success(earnings) {
  return {
    type: EARNINGS,
    payload: {
      status: 'success',
      earnings
    }
  };
}

function getEarnings() {
  return function(dispatch) {
    dispatch(begin());
    return fetchApi({ path: EARNINGS_PATH })
      .then(response => dispatch(success(response.data)))
      .catch(error => dispatch(failure(error)));
  };
}

export { reset, begin, failure, success, getEarnings };
