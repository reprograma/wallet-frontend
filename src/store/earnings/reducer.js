import { EARNINGS } from './constants';

const initialState = {
  status: 'initial',
  earnings: {},
  error: null
};

function earningsReducer(state = initialState, action) {
  if (action.type === EARNINGS) {
    const { status, earnings, error } = action.payload;

    return {
      ...state,
      status,
      earnings: earnings || state.earnings,
      error: error || state.error
    };
  }

  return state;
}

export { earningsReducer };
