import { combineReducers } from 'redux';

// reducers namespaces
import { walletActionsReducer as walletActions } from 'components/widgets/WalletActions/reducer';
import { walletReducer as wallet } from './wallet/reducer';
import { userReducer as user } from './user/reducer';
import { loginReducer as login } from './login/reducer';
import { currencyValuesReducer as currencyValues } from './currency-values/reducer';
import { earningsReducer as earnings } from './earnings/reducer';
import { monthEarningsReducer as monthEarnings } from './month-earnings/reducer';
import { transactionsReducer as transactions } from './transactions/reducer';
import { lastTransactionsReducer as lastTransactions } from './last-transactions/reducer';

// TODO: refactor & move to ./wallet-actions

const reducers = {
  login,
  currencyValues,
  earnings,
  monthEarnings,
  transactions,
  lastTransactions,
  user,
  wallet,
  walletActions
};

export default combineReducers(reducers);
