import { fetchApi, tokenName } from 'utils';
import { LOGIN } from './constants';

const CLIENT_ID = process.env.REACT_APP_CLIENT_ID;
const CLIENT_SECRET = process.env.REACT_APP_CLIENT_SECRET;
const LOGIN_PATH = process.env.REACT_APP_LOGIN_PATH;

function reset() {
  return {
    type: LOGIN,
    payload: {
      status: 'initial'
    }
  };
}

function begin() {
  return {
    type: LOGIN,
    payload: {
      status: 'pending'
    }
  };
}

function failure(error) {
  return {
    type: LOGIN,
    payload: {
      status: 'error',
      error
    },
    error: true
  };
}

function success(token) {
  window.localStorage[tokenName] = token;
  return {
    type: LOGIN,
    payload: {
      status: 'success',
      token
    }
  };
}

function login(username, password, code) {
  return function(dispatch) {
    dispatch(begin());
    // snake_case for the API
    const body = {
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      pin: code,
      grant_type: 'password',
      scope: 'panel',
      username,
      password
    };
    return fetchApi({
      body,
      path: LOGIN_PATH,
      method: 'post'
    })
      .then(response => dispatch(success(response.access_token)))
      .catch(error => dispatch(failure(error)));
  };
}

export { reset, begin, success, failure, login };
