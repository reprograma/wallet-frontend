import { LOGIN } from './constants';

const initialState = {
  status: 'initial',
  token: null,
  error: null
};

function loginReducer(state = initialState, action) {
  if (action.type === LOGIN) {
    const { token, error, status } = action.payload;

    return {
      ...state,
      status,
      token,
      error
    };
  }

  return state;
}

export { loginReducer };
