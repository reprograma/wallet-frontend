import { LAST_TRANSACTIONS } from './constants';

const initialState = {
  status: 'initial',
  transactions: [],
  error: null
};

function lastTransactionsReducer(state = initialState, action) {
  if (action.type === LAST_TRANSACTIONS) {
    const { status, transactions, error } = action.payload;

    return {
      ...state,
      status,
      transactions: transactions || state.transactions,
      error: error || state.error
    };
  }

  return state;
}

export { lastTransactionsReducer };
