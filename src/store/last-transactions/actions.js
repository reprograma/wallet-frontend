import { fetchApi } from 'utils';
import { LAST_TRANSACTIONS } from './constants';

const LAST_TRANSACTIONS_PATH = process.env.REACT_APP_LAST_TRANSACTIONS_PATH;

function reset() {
  return {
    type: LAST_TRANSACTIONS,
    payload: {
      status: 'initial'
    }
  };
}

function begin() {
  return {
    type: LAST_TRANSACTIONS,
    payload: {
      status: 'pending'
    }
  };
}

function failure(error) {
  return {
    type: LAST_TRANSACTIONS,
    payload: {
      status: 'error',
      error
    },
    error: true
  };
}

function success(transactions) {
  return {
    type: LAST_TRANSACTIONS,
    payload: {
      status: 'success',
      transactions
    }
  };
}

function getLastTransactions() {
  return function(dispatch) {
    dispatch(begin());
    return fetchApi({ path: LAST_TRANSACTIONS_PATH })
      .then(response => dispatch(success(response.data)))
      .catch(error => dispatch(failure(error)));
  };
}

export { reset, begin, failure, success, getLastTransactions };
