import { MONTH_EARNINGS } from './constants';

const initialState = {
  status: 'initial',
  monthEarnings: {},
  error: null
};

function monthEarningsReducer(state = initialState, action) {
  if (action.type === MONTH_EARNINGS) {
    const { status, monthEarnings, error } = action.payload;

    return {
      ...state,
      status,
      monthEarnings: monthEarnings || state.monthEarnings,
      error: error || state.error
    };
  }

  return state;
}

export { monthEarningsReducer };
