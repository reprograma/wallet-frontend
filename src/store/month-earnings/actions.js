import { fetchApi } from 'utils';
import { MONTH_EARNINGS } from './constants';

const MONTH_EARNINGS_PATH = process.env.REACT_APP_MONTH_EARNINGS_PATH;

function reset() {
  return {
    type: MONTH_EARNINGS,
    payload: {
      status: 'initial'
    }
  };
}

function begin() {
  return {
    type: MONTH_EARNINGS,
    payload: {
      status: 'pending'
    }
  };
}

function failure(error) {
  return {
    type: MONTH_EARNINGS,
    payload: {
      status: 'error',
      error
    },
    error: true
  };
}

function success(monthEarnings) {
  return {
    type: MONTH_EARNINGS,
    payload: {
      status: 'success',
      monthEarnings
    }
  };
}

function getMonthEarnings() {
  return function(dispatch) {
    dispatch(begin());
    return fetchApi({ path: MONTH_EARNINGS_PATH })
      .then(response => dispatch(success(response.data)))
      .catch(error => dispatch(failure(error)));
  };
}

export { reset, begin, failure, success, getMonthEarnings };
