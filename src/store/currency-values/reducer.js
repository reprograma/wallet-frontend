import { CURRENCY_VALUES } from './constants';

const initialState = {
  status: 'initial',
  values: {},
  error: null
};

function currencyValuesReducer(state = initialState, action) {
  if (action.type === CURRENCY_VALUES) {
    const { status, values, error } = action.payload;

    return {
      ...state,
      status,
      values: values || state.values,
      error: error || state.error
    };
  }

  return state;
}

export { currencyValuesReducer };
