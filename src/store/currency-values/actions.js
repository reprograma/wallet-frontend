import { fetchApi } from 'utils';
import { CURRENCY_VALUES } from './constants';

const CURRENCY_VALUES_PATH = process.env.REACT_APP_CURRENCY_VALUES_PATH;

function reset() {
  return {
    type: CURRENCY_VALUES,
    payload: {
      status: 'initial'
    }
  };
}

function begin() {
  return {
    type: CURRENCY_VALUES,
    payload: {
      status: 'pending'
    }
  };
}

function failure(error) {
  return {
    type: CURRENCY_VALUES,
    payload: {
      status: 'error',
      error
    },
    error: true
  };
}

function success(values) {
  return {
    type: CURRENCY_VALUES,
    payload: {
      status: 'success',
      values
    }
  };
}

function getValues() {
  return function(dispatch) {
    dispatch(begin());
    return fetchApi({ path: CURRENCY_VALUES_PATH })
      .then(response => dispatch(success(response.data)))
      .catch(error => dispatch(failure(error)));
  };
}

export { reset, begin, failure, success, getValues };
