import { WALLET } from './constants';

const initialState = { status: 'initial', wallet: [], error: null };

function walletReducer(state = initialState, action) {
  if (action.type === WALLET) {
    const { status, wallet, error } = action.payload;

    return {
      status,
      wallet: wallet || state.wallet,
      error: error || state.error
    };
  }

  return state;
}

export { walletReducer };
