import { fetchApi, logOut } from 'utils';
import { WALLET } from './constants';

const USER_WALLET_PATH = process.env.REACT_APP_USER_WALLET_PATH;

function reset() {
  return {
    type: WALLET,
    payload: {
      status: 'initial'
    }
  };
}

function begin() {
  return {
    type: WALLET,
    payload: {
      status: 'pending'
    }
  };
}

function failure(error) {
  return {
    type: WALLET,
    payload: {
      status: 'error',
      error
    },
    error: true
  };
}

function success(wallet) {
  return {
    type: WALLET,
    payload: {
      status: 'success',
      wallet
    }
  };
}

function getWallet() {
  return function getWalletThunk(dispatch) {
    dispatch(begin());
    return fetchApi({ path: USER_WALLET_PATH })
      .then(response => dispatch(success(response.data)))
      .catch(error => {
        logOut();
        dispatch(failure(error));
      });
  };
}

export { reset, begin, success, failure, getWallet };
