import { fetchApi, logOut } from 'utils';
import { USER } from './constants';

const USER_ACCOUNT_PATH = process.env.REACT_APP_USER_ACCOUNT_PATH;

function reset() {
  return {
    type: USER,
    payload: {
      status: 'initial'
    }
  };
}

function begin() {
  return {
    type: USER,
    payload: {
      status: 'pending'
    }
  };
}

function failure(error) {
  return {
    type: USER,
    payload: {
      status: 'error',
      error
    },
    error: true
  };
}

function success(user) {
  return {
    type: USER,
    payload: {
      status: 'success',
      user
    }
  };
}

function getUser() {
  return function(dispatch) {
    dispatch(begin());
    return fetchApi({ path: USER_ACCOUNT_PATH })
      .then(response => dispatch(success(response.data)))
      .catch(error => {
        logOut();
        dispatch(failure(error));
      });
  };
}

export { reset, begin, success, failure, getUser };
