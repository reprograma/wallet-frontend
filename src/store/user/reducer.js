import { USER } from './constants';

const initialState = {
  status: 'initial',
  user: {},
  error: null
};

function userReducer(state = initialState, action) {
  if (action.type === USER) {
    const { status, user, error } = action.payload;

    return {
      status,
      user: user || state.user,
      error: error || state.error
    };
  }

  return state;
}

export { userReducer };
