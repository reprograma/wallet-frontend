import React from 'react';
import { Provider } from 'react-redux';
import store from 'store';
import MainRoutes from 'components/routes/MainRoutes';
import 'App.css';

const App = () => (
  <Provider store={store}>
    <MainRoutes />
  </Provider>
);

export default App;
